<?php

// Tell whomever is receiving this data the content type
// Needs to be set on the first line
// header('Content-Type: application/json');
$_POST = json_decode(file_get_contents('php://input'), true); //sirve para convertir a json lo que viene
// Insert to the PacienteVO class
require_once('../../vo/PacienteVO.php');

// Get a connection to the database
require_once('../../conexionDB/mysqli_connect.php');

// Check the connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

	/* Will hold all MEDICO retreived
	if (mysqli_num_rows($dbc->query($query)) == 1) {
		echo "tiene una fila";
		$row = mysqli_fetch_array($dbc->query($query));
		echo $row['usuario'];
	}else echo "no tiene fila";
	*/

$user_idMedico = $_POST['idMedico'];
// Query retrieves the medico data
	$query = "SELECT * FROM paciente WHERE idMedico = '$user_idMedico'";
		$paciente_array = array();
		if($result = $dbc->query($query)){
			$row = mysqli_fetch_array($dbc->query($query));
				while ($obj = $result->fetch_object()){
					$temp_paciente = new PacienteVO((int)$obj->idPaciente,$obj->nombre,$obj->apellidoPaterno, $obj->apellidoMaterno, $obj->estadoCivil,$obj->fechaNacimiento, $obj->edad, $obj->email, $obj->telParticular, $obj->celular, $obj->sexo, $obj->direccion, $obj->grupoSanguineo, $obj->activo, $obj->observaciones, $obj->idMedico);
					
					$paciente_array[] = $temp_paciente;
				
				}

				// Take data array created and convert into JSON
				if($paciente_array != null){
				$dale_data = json_encode($paciente_array);
				echo $dale_data;
				}
		


			// Close the database connection
			$result->close();
			$dbc->close();

		}
?>