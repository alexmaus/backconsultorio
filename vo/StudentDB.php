<?php

class StudentDB {

	public $student_id;
	public $first_name = "";
	public $last_name = "";
	public $email = "";
	public $street = "";
	public $city = "";
	public $state = "";
	
	
	function __construct($studentid,$first_name, $last_name, $email, $street,
		$city, $state){

		$this->student_id = $studentid;
		$this->first_name = $first_name;
		$this->last_name = $last_name;
		$this->email = $email;
		$this->street = $street;
		$this->city = $city;
		$this->state = $state;
		
	
	}

}

?>