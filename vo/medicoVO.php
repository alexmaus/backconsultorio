<?php

class MedicoVO {

	public $idMedico;
	public $nombre = "";
	public $apellidoPaterno = "";
	public $apellidoMaterno = "";
	public $numCedula;
	public $telefono = "";
	public $celular = "";
	public $usuario = "";
	public $contrasenia = "";
	
	
	function __construct($idMedico,$nombre, $apellidoPaterno, $apellidoMaterno, $numCedula,
		$telefono, $celular,$usuario,$contrasenia){

		$this->idMedico = $idMedico;
		$this->nombre = $nombre;
		$this->apellidoPaterno = $apellidoPaterno;
		$this->apellidoMaterno = $apellidoMaterno;
		$this->numCedula = $numCedula;
		$this->telefono = $telefono;
		$this->celular = $celular;
		$this->usuario = $usuario;
		$this->contrasenia = $contrasenia;
		
	
	}

}

?>