<?php

class PacienteVO {

	public $idPaciente;
	public $nombre = "";
	public $apellidoPaterno = "";
	public $apellidoMaterno = "";
	public $estadoCivil;
	public $fechaNacimiento = "";
	public $edad;
	public $email = "";
	public $telParticular = "";
	public $celular = "";
	public $sexo = "";
	public $direccion = "";
	public $grupoSanguineo = "";
	public $activo;
	public $observaciones = "";
	public $idMedico;
	
	
	function __construct($idPaciente,$nombre, $apellidoPaterno, $apellidoMaterno, $estadoCivil,$fechaNacimiento, $edad,$email,$telParticular,$celular,$sexo,$direccion,$grupoSanguineo,$activo,$observaciones,$idMedico){

		$this->idPaciente = $idPaciente;
		$this->nombre = $nombre;
		$this->apellidoPaterno = $apellidoPaterno;
		$this->apellidoMaterno = $apellidoMaterno;
		$this->estadoCivil = $estadoCivil;
		$this->fechaNacimiento = $fechaNacimiento;
		$this->edad = $edad;
		$this->email = $email;
		$this->telParticular = $telParticular;
		$this->celular = $celular;
		$this->sexo = $sexo;
		$this->direccion = $direccion;
		$this->grupoSanguineo = $grupoSanguineo;
		$this->activo = $activo;
		$this->observaciones = $observaciones;
		$this->idMedico = $idMedico;
	}

}

?>